# Ethereum node without PoA participation.

Automated deploying of an Ethereum node (that doesn't participate in the PoA consensus algorithm) to our network, using Docker.

## Deployment

Run the following commands from [this directory](poaBlockchain/extraNode) in order:

```javascript
$ docker-compose -f initialize-node.yml up  //initializes node with the genesis block file
$ docker-compose up //deploys the docker container
```


